//je vais chercher mon bloc et mon titre
var bannerContent = document.getElementById("banner-content");
var bannerTitle = document.getElementById("banner-title");
//date du jour
var adate = new Date();
//avoir le numéro du jour 0 lundi , 6 dimanche
var date = adate.getDate();
//changement de l'image et texte chaque jour
if (date == 0) {
  date = "lundi";
  bannerTitle.innerHTML = `Comme un lundi , comme dirait Pascal`;
  bannerContent.innerHTML = `<img class="banner-image" src="./images/pane.jpeg">`;
} else if (date == 1) {
  date = "mardi";
  bannerTitle.innerHTML = `C'est Mardi`;
  bannerContent.innerHTML = `<img class="banner-image" src="./images/codepromo.jpg">`;
} else if (date == 2) {
  date = "mercredi";
  bannerTitle.innerHTML = `Code Promo Today : AFPA20`;
  bannerContent.innerHTML = `<img class="banner-image" src="./images/blackfriday2.jpg">`;
} else if (date == 3) {
  date = "jeudi";
  bannerTitle.innerHTML = `Code Promo : la Saint du jour `;
  bannerContent.innerHTML = `<img class="banner-image" src="./images/blackfriday.jpg">`;
} else if (date == 4) {
  date = "vendredi";
  bannerTitle.innerHTML = `Vendredi tout est permis !`;
  bannerContent.innerHTML = `<img class="banner-image" src="./images/vtep.png">`;
} else if (date == 5) {
  date = "samedi";
  bannerTitle.innerHTML = `Plus d'idées dans les codes promos ...`;
} else if (date == 6) {
  date = "dimanche";
  bannerTitle.innerHTML = `Aujourd'hui on se repose !`;
}
