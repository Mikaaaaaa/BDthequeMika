function maSuperFonctionjs() {
  var login = document.getElementById("login").value;
  location.replace(encodeURI("accueil.html?l=" + login));
}
// découper sur ? ==> tableau à 2 postes
var infos = decodeURI(location.href).split("?")[1];
// découper sur & ==> tableau à n postes (ici n=2)
// (on pouvait aussi parcourir le tableau tab avec une boucle for)
var tab = infos.split("&");
// les valeurs sont dans le 2° poste de chacun des tableaux
var txt = decodeURI(tab[0].split("=")[1]);
// afficher dans le span
document.getElementById(
  "recu"
).innerHTML += `<span class="namelogin">${txt}</span>`;
document.getElementById("recu-mobile").innerHTML = `<h3>${txt}</h3>`;
var mdp = document.getElementById("mdp");
//guest n'aura pas de panier. Si il clique le panier, on l'invitera à se connecter
if (txt == "guest") {
  document.getElementById("panier").innerHTML =
    "<a href='index.html'><i id='panier'class='fa fa-cart-arrow-down fa-2x'</i></a>";
  document.getElementById("container-panier").style.display = "none";
} else {
  document.getElementById("panier").innerHTML =
    "<a href='#'><i id='panier'class='fa fa-cart-arrow-down fa-2x'</i></a>";
}
