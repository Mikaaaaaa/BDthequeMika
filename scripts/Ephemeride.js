var dd1, delai;
function debuteDate1(delai1) {
  var adate, date, amois;
  delai = delai1;
  adate = new Date();
  date = adate.getDate();
  amois = adate.getMonth() + 1;
  if (amois == 1) date += " Janvier";
  else if (amois == 2) date += " Février";
  else if (amois == 3) date += " Mars";
  else if (amois == 4) date += " Avril";
  else if (amois == 5) date += " Mai";
  else if (amois == 6) date += " Juin";
  else if (amois == 7) date += " Juillet";
  else if (amois == 8) date += " Août";
  else if (amois == 9) date += " Septembre";
  else if (amois == 10) date += " Octobre";
  else if (amois == 11) date += " Novembre";
  else if (amois == 12) date += " Décembre";
  if (adate.getYear() > "99") date += " " + adate.getYear();
  else date += "  " + (1900 + adate.getYear());
  date = "  " + date;
  document.Tempsh.date.value = date;
  dd1 = setTimeout("debuteDate1(delai)", delai1);
}
function getFonc(mois, jour) {
  var ar = new Array(12);
  ar[0] = getFete01(jour);
  ar[1] = getFete02(jour);
  ar[2] = getFete03(jour);
  ar[3] = getFete04(jour);
  ar[4] = getFete05(jour);
  ar[5] = getFete06(jour);
  ar[6] = getFete07(jour);
  ar[7] = getFete08(jour);
  ar[8] = getFete09(jour);
  ar[9] = getFete10(jour);
  ar[10] = getFete11(jour);
  ar[11] = getFete12(jour);
  return ar[mois];
}

function getFete01(num) {
  var fetes = new Array(
    "Jour de l'An",
    "Basile",
    "Geneviève",
    "Odilon",
    "Edouard",
    "Melaine",
    "Raymond",
    "Lucien",
    "Alix",
    "Guillaume",
    "Paulin",
    "Tatiana",
    "Yvette",
    "Nina",
    "Rémi",
    "Marcel",
    "Roseline",
    "Prisca",
    "Marius",
    "Sébastien",
    "Agnès",
    "Vincent",
    "Barnard",
    "François",
    "Paul",
    "Paule",
    "Angèle",
    "Thomas",
    "Gildas",
    "Martine",
    "Marcelle",
    "",
    "",
    ""
  );
  return fetes[num];
}

function getFete02(num) {
  var fetes = new Array(
    "Ella",
    "Présention",
    "Blaise",
    "Véronique",
    "Agathe",
    "Gaston",
    "Eugènie",
    "Jacqueline",
    "Apolline",
    "Arnaud",
    "ND Lourdes",
    "Félix",
    "Béatrice",
    "Valentin",
    "Claude",
    "Julienne",
    "Alexis",
    "Bernadette",
    "Gabin",
    "Aimée",
    "Cendres",
    "Isabelle",
    "Lazare",
    "Modeste",
    "Roméo",
    "Nestor",
    "Honorine",
    "Romain",
    "Auguste",
    "",
    "",
    "",
    "",
    ""
  );
  return fetes[num];
}

function getFete03(num) {
  var fetes = new Array(
    "Aubin",
    "Charles",
    "Guénolé",
    "Casimir",
    "Olive",
    "Colette",
    "Félicité",
    "Jean",
    "Françoise",
    "Vivien",
    "Rosine",
    "Justine",
    "Rodrigue",
    "Mathilde",
    "Louise",
    "Bénédicte",
    "Patrice",
    "Cyrille",
    "Joseph",
    "Herbert",
    "Clémence",
    "Léa",
    "Victorien",
    "Karine",
    "Ann.",
    "Larissa",
    "Habib",
    "Gontran",
    "Gwladys",
    "Amédée",
    "Benjamin",
    "",
    "",
    ""
  );
  return fetes[num];
}

function getFete04(num) {
  var fetes = new Array(
    "Hugues",
    "Sandrine",
    "Richard",
    "Isidore",
    "Irène",
    "Marcellin",
    "JB.de la S",
    "Julie",
    "Gautier",
    "Fulbert",
    "Stanislas",
    "Jules",
    "Ida",
    "Maxime",
    "Paterne",
    "Benoït",
    "Anicet",
    "Parfait",
    "Emma",
    "Odette",
    "Anselme",
    "Alexandre",
    "Georges",
    "Fidèle",
    "Marc",
    "Alida",
    "Zita",
    "Valérie",
    "Catherine",
    "Robert",
    "",
    "",
    ""
  );
  return fetes[num];
}

function getFete05(num) {
  var fetes = new Array(
    "Fête du travail",
    "Boris",
    "Philippe",
    "Sylvain",
    "Judith",
    "Prudence",
    "Gisèle",
    "Armistice 1945",
    "Pâcome",
    "Solange",
    "Estelle",
    "Achille",
    "Rolande",
    "Matthias",
    "Denise",
    "Honoré",
    "Pascal",
    "Eric",
    "Yves",
    "Bernardin",
    "Constantin",
    "Emile",
    "Didier",
    "Donatien",
    "Sophie",
    "Béranger",
    "Augustin",
    "Germain",
    "Aymar",
    "Ferdinand",
    "Visitation",
    "",
    "",
    ""
  );
  return fetes[num];
}

function getFete06(num) {
  var fetes = new Array(
    "Justin",
    "Blandine",
    "Kévin",
    "Clotilde",
    "Igor",
    "Norbert",
    "Gilbert",
    "Médard",
    "Diane",
    "Landry",
    "Yolande",
    "Guy",
    "Antoine",
    "Elisée",
    "Germaine",
    "J.F.Régis",
    "Hervé",
    "Léonce",
    "Romuald",
    "Silvère",
    "Rodolphe",
    "Alban",
    "Audrey",
    "Baptiste",
    "Prosper",
    "Anthelme",
    "Fernand",
    "Irénée",
    "Paul/Pierre",
    "Martial",
    "",
    "",
    ""
  );
  return fetes[num];
}

function getFete07(num) {
  var fetes = new Array(
    "Thierry",
    "Martinien",
    "Thomas",
    "Florent",
    "Antoine",
    "Mariette",
    "Raoul",
    "Thibaut",
    "Amandine",
    "Ulrich",
    "Benoït",
    "Olivier",
    "Joël",
    "Camille",
    "Donald",
    "ND.Carmel",
    "Caroline",
    "Frédéric",
    "Arsène",
    "Marina",
    "Victor",
    "Madeleine",
    "Brigitte",
    "Christine",
    "Jacques",
    "Anne",
    "Nathalie",
    "Samson",
    "Marthe",
    "Juliette",
    "Ignace",
    "",
    "",
    ""
  );
  return fetes[num];
}

function getFete08(num) {
  var fetes = new Array(
    "Alphonse",
    "Julien",
    "Lydie",
    "Jean Marie",
    "Oswald",
    "Transf.",
    "Gaétan",
    "Dominique",
    "Amour",
    "Laurent",
    "Claire",
    "Clarisse",
    "Hippolyte",
    "Evrard",
    "Alfred",
    "Armel",
    "Hyacinthe",
    "Hélène",
    "Jean",
    "Bernard",
    "Christophe",
    "Fabrice",
    "Rose",
    "Barthélémy",
    "Louis",
    "Natacha",
    "Monique",
    "Augustin.",
    "Sabine",
    "Fiacre",
    "Aristide",
    "",
    "",
    ""
  );
  return fetes[num];
}

function getFete09(num) {
  var fetes = new Array(
    "Gilles",
    "Ingrid",
    "Grégoire",
    "Rosalie",
    "Raïssa",
    "Bertrand",
    "Reine",
    "Nativité",
    "Alain",
    "Inés",
    "Adelphe",
    "Apollinaire",
    "Aimé",
    "Ste-Croix",
    "Roland",
    "Edith",
    "Renaud",
    "Nadège",
    "Amélie",
    "Davy",
    "Mathieu",
    "Maurice",
    "Constant",
    "Thècle",
    "Hermann",
    "Côme",
    "Vincent",
    "Venceslas",
    "Gabriel",
    "Jérôme",
    "",
    "",
    ""
  );
  return fetes[num];
}

function getFete10(num) {
  var fetes = new Array(
    "Thérèse",
    "Léger",
    "Gérard",
    "François",
    "Fleur",
    "Bruno",
    "Serge",
    "Pélagie",
    "Denis",
    "Ghislain",
    "Firmin",
    "Wilfried",
    "Géraud",
    "Juste",
    "Thérèse",
    "Edwige",
    "Baudoin",
    "Luc",
    "René",
    "Adeline",
    "Céline",
    "Elodie",
    "Jean",
    "Florentin",
    "Enguerran",
    "Dimitri",
    "Emeline",
    "Simon",
    "Narcisse",
    "Bienvenue",
    "Quentin",
    "",
    "",
    ""
  );
  return fetes[num];
}

function getFete11(num) {
  var fetes = new Array(
    "Toussaint",
    "Défunts",
    "Hubert",
    "Charles",
    "Sylvie",
    "Bertille",
    "Carine",
    "Geoffroy",
    "Théodore",
    "Léon",
    "Martin",
    "Christian",
    "Brice",
    "Sidoine",
    "Albert",
    "Marguerite",
    "Elisabeth",
    "Aude",
    "Tanguy",
    "Edmond",
    "Présentat.",
    "Cécile",
    "Clément",
    "Augusta",
    "Catherine",
    "Delphine",
    "Séverin",
    "Jacques",
    "Saturnin",
    "André",
    "",
    "",
    ""
  );
  return fetes[num];
}

function getFete12(num) {
  var fetes = new Array(
    "Florence",
    "Viviane",
    "François",
    "Barbara",
    "Gérald",
    "Nicolas",
    "Ambroise",
    "Imm.Conc.",
    "Pierre",
    "Romaric",
    "Daniel",
    "Chantal",
    "Lucie",
    "Odile",
    "Ninon",
    "Alice",
    "Gaël",
    "Gatien",
    "Urbain",
    "Abraham",
    "Pierre",
    "Françoise",
    "Armand",
    "Adèle",
    "Noël",
    "Etienne",
    "Jean",
    "Innocents",
    "David",
    "Roger",
    "Sylvestre",
    "",
    "",
    ""
  );
  return fetes[num];
}
