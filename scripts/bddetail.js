//ya les bons copieurs , ceux qui copient
//et les mauvais copieurs, alors eux... ils copient et ... voila 
var comboAuteur = document.FormAuteur.ListeAuteurs;
var comboAlbum = document.FormAuteur.ListeAlbums;
var monAffichage = document.getElementById("pouraffichage");
var auteurSelection = document.getElementById("maListeAuteur");
var albumSelection = document.getElementById("maListeAlbum");
var idAuteurSelectionne = 0;
var auteurSelectionner = "";

function fieldComboAuteur(value) {
  comboAuteur.length++;
  comboAuteur.options[comboAuteur.length - 1].text = value.nom;
}

function fieldComboAlbum(value) {
  var numAuteur = value.idAuteur;

  if (numAuteur == idAuteurSelectionne) {
    comboAlbum.length++;
    comboAlbum.options[comboAlbum.length - 1].text = value.titre;
  }
}
function searchIdAuteur(value, key) {
  if (value.nom == auteurSelectionner) {
    idAuteurSelectionne = key;
  }
}

function getAuteur(element) {
  monAffichage.innerHTML += "Auteur = " + element.value;
  auteurSelectionner = element.value;
  auteurs.forEach(searchIdAuteur);
  comboAlbum.length = 1;
  albums.forEach(fieldComboAlbum);
}

function getAlbum(element) {
  monAffichage.innerHTML += " Album = " + element.value;
}

auteurs.forEach(fieldComboAuteur);

auteurSelection.addEventListener("change", function () {
  monAffichage.innerHTML = "Votre choix : ";
  getAuteur(this);
});

albumSelection.addEventListener("change", function () {
  getAlbum(this);
});
