var cartadd = document.querySelectorAll(".cartadd");
//listing des produits sous forme d'objets
let produits = [
  {
    name: "Astérix et Cléopatre",
    tag: "art1",
    prix: 9.95,
    inCart: 0,
  },
  {
    name: "Astérix et les Goths",
    tag: "art2",
    prix: 10.95,
    inCart: 0,
  },
];

for (let index = 0; index < cartadd.length; index++) {
  cartadd[index].addEventListener("click", () => {
    monPanier(produits[index]);
  });
}
// me retourne nom et prix dans l'encart du panier au clique sur le bouton cartadd
function monPanier(monProduit) {
  document.getElementById(
    "nbarticle"
  ).innerHTML += `<li class="elem">${monProduit.name} <br> ${monProduit.prix} € <br><hr></li>`;
}
//suppression élément dans le panier
$(document).ready(function () {
  $(".suppr").click(function () {
    $(".elem").remove();
  });
});
