//declaration de mes variables
var login = document.getElementById("login");
var mdp = document.getElementById("mdp");
var validation = document.getElementById("soumission");
var blocForm = document.getElementById("bloc-form");
var returnbutton = document.getElementById("return");

function verifForm() {
  if (login.value !== "") {
    //login
    if (login.value == mdp.value) {
      var bFormulaireOK;
      document.getElementById("maForme").submit();
      maSuperFonctionjs();
      //guest
    } else if (login.value == "guest") {
      bFormulaireOK = true;
      document.getElementById("maForme").submit();
      maSuperFonctionjs();
      //admin
    } else if (login.value == "admin" && mdp.value == "secret") {
      bFormulaireOK = true;
      document.getElementById("maForme").submit();
      maSuperFonctionjs();
    }
  } else {
    bFormulaireOK = false;
    blocForm.style.display = "block";
    //pop-up du bloc qui bounce avec le lien pour revenir a la page index.html
    blocForm.innerHTML = "Saisissez au moins votre Login !";
    returnbutton.style.display = "block";
    //alert("saisissez au moins votre login");
  }
}
validation.addEventListener("click", verifForm, false);
